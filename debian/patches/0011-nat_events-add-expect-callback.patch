From 0040f0fa80e1ab84dc1487c30b89da17634eec94 Mon Sep 17 00:00:00 2001
From: Vadim Fedorenko <vfedorenko@novek.ru>
Date: Mon, 28 Mar 2022 22:41:33 +0300
Subject: [PATCH 11/17] nat_events: add expect callback

Starting with 5.15 kernel uses the same notifier structure for
actual nat events and additional expect events for nat helpers.
Expect event callback is assumed to exist and the only thing that
could disable such events is netlink configuration from user space.
That's why this module have to provide such callback, otherwise
kernel panic is expected to happen if any nat helper is enabled
(most common is pptp gre helper).

Signed-off-by: Vadim Fedorenko <vfedorenko@novek.ru>
Link: https://github.com/aabc/ipt-netflow/pull/196
---
 ipt_NETFLOW.c | 18 +++++++++++++++++-
 1 file changed, 17 insertions(+), 1 deletion(-)

diff --git a/ipt_NETFLOW.c b/ipt_NETFLOW.c
index 82805bc..f5ee676 100644
--- a/ipt_NETFLOW.c
+++ b/ipt_NETFLOW.c
@@ -4597,6 +4597,19 @@ static void rate_timer_calc(
 #ifdef CONFIG_NF_NAT_NEEDED
 #if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,31)
 static struct nf_ct_event_notifier *saved_event_cb __read_mostly = NULL;
+#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,15,0)
+static int netflow_conntrack_expect_event(const unsigned int events, const struct nf_exp_event *item)
+{
+	struct nf_ct_event_notifier *notifier;
+
+	/* Call netlink first. */
+	notifier = rcu_dereference(saved_event_cb);
+	if (likely(notifier))
+		return notifier->exp_event(events, item);
+	else
+		return 0;
+}
+#endif
 static int netflow_conntrack_event(const unsigned int events, NF_CT_EVENT *item)
 #else
 static int netflow_conntrack_event(struct notifier_block *this, unsigned long events, void *ptr)
@@ -4684,7 +4697,10 @@ static struct notifier_block ctnl_notifier = {
 };
 #else
 static struct nf_ct_event_notifier ctnl_notifier = {
-	.ct_event = netflow_conntrack_event
+	.ct_event = netflow_conntrack_event,
+#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,15,0)
+	.exp_event = netflow_conntrack_expect_event,
+#endif
 };
 #endif /* since 2.6.31 */
 #endif /* CONFIG_NF_NAT_NEEDED */
-- 
2.39.5

